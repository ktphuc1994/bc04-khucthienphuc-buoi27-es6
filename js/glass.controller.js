export let renderGlasses = (dataGlasses) => {
  let glassesListDiv = document.getElementById("vglassesList");
  glassesListDiv.innerHTML = "";
  dataGlasses.forEach((glass) => {
    let glassDiv = document.createElement("div");
    glassDiv.classList.add("col-4");
    glassDiv.innerHTML = `<img src="${glass.src}" alt="${glass.id}" class="vglasses__items" />`;
    glassesListDiv.append(glassDiv);
    glassDiv.children[0].addEventListener("click", tryGlassControl.tryGlass);
  });
};
export let tryGlassControl = {
  tryGlass: () => {
    console.log("yes");
  },
};
export let renderGlassInfo = (glassIndex, dataGlasses) => {
  let { id, src, virtualImg, brand, name, color, price, description } =
    dataGlasses[glassIndex];
  let glassInfoContent = `<div class="glassesInfo__content">
  <p class="font-weight-bold"><span class="text-uppercase">${name}</span> - ${brand} (${color})</p>
  <p class="text-success"><span class="price bg-danger text-white">$${price}</span>Stocking</p>
  <p>${description}</p>
  </div>`;
  document.getElementById("glassesInfo").innerHTML = glassInfoContent;
  document.getElementById("glassesInfo").style.display = "block";
};
