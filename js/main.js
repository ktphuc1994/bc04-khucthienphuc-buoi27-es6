import { dataGlasses } from "../data/data.js";
import {
  renderGlasses,
  renderGlassInfo,
  tryGlassControl,
} from "./glass.controller.js";

tryGlassControl.tryGlass = (e) => {
  //   console.log("yes");
  let id = e.target.alt;
  let index = dataGlasses.findIndex((glass) => {
    return glass.id == id;
  });
  //   console.log(index);
  renderGlassInfo(index, dataGlasses);
  document.getElementById(
    "avatar"
  ).innerHTML = `<img src="${dataGlasses[index].virtualImg}" id="glasses" />`;
  document.getElementById("glasses").style.display = "inline";
};

renderGlasses(dataGlasses);

let removeGlasses = (isGlassOn) => {
  if (isGlassOn) {
    document.getElementById("glasses").style.opacity = 0.9;
  } else {
    document.getElementById("glasses").style.opacity = 0;
  }
};
window.removeGlasses = removeGlasses;
